import { readFileSync } from 'fs'
import { parse } from 'svg-parser'
import svgpathparser from 'svg-path-parser'
const { parseSVG } = svgpathparser

if (process.argv.length <= 2) process.exit('Usage: node ./index.js <FILE>')
const name = process.argv[2]
const file = readFileSync(name).toString()
const parsed = parse(file)

let result = 'let xPos = 0;\nlet yPos = 0;\nhide()\n'

const error = (err) => {
    console.log(`\n${result}`)
    throw new Error(err)
}

const parseSvg = (data) => {
    switch (data.type) {
        case 'root':
            console.log('found root node')
            break
        case 'element':
            switch (data.tagName) { 
                case 'svg':
                    console.log('found element node (svg)')
                    result += `createCanvas('${name}', ${data.properties.width.replace(/[^0-9.]/g,'')}, ${data.properties.height.replace(/[^0-9.]/g,'')});\n`
                    result += `setActiveCanvas('${name}')\n`
                    break
                case 'g':
                    console.log('found element node (g)')
                    if (data.properties.transform) {
                        const transforms = data.properties.transform.split(' ')                        
                        transforms.forEach(transform => {
                            if (transform.startsWith('translate')) {
                                const split = transform.split('(')[1].replace(')','').split(',')
                                const translateX = split[0]
                                const translateY = split[1]
                                result += `move(${translateX}, ${translateY});\n`
                            } else if (transform.startsWith('scale')) {
                                console.log('Not doing intentionally unimplemented \'scale\' transform.')
                                result += '// TODO: Implement \'scale\' transform.'
                            } else {
                                error(`Invalid SVG Group Transform '${transform}'`)
                            }
                        })
                    }
                    break
                case 'path':
                    console.log('found element node (path)')
                    const path = data.properties.d
                    const parsedPath = parseSVG(path)
                    parsedPath.forEach(command => {
                        switch (command.command) {
                            case 'moveto':
                                console.log('found path command \'moveto\'')
                                if (command.relative) {
                                    console.log('   is relative')
                                    result += `move(${command.x}, ${command.y})`
                                } else {
                                    console.log('   is not relative')
                                    result += `moveTo(${command.x}, ${command.y})`
                                }
                                break
                            case 'lineto':
                                console.log('found path command \'lineto\'')
                                if (command.relative) {
                                    console.log('   is relative')
                                } else {
                                    console.log('   is not relative')
                                    result += `xPos = getX()`
                                    result += `xPos = getY()`
                                    result += `penDown()`
                                    result += `moveTo()`
                                }
                                break
                            default:
                                error(`Invalid SVG path command '${command.command}'`)
                        }
                    })
                    error('temp')
                    break
                default:
                    error(`Invalid SVG element type '${data.tagName}'`)
            }
            break
        default:
            error(`Invalid SVG data type '${data.type}'`)
    }
    if (data.children) data.children.forEach(child => parseSvg(child))
}

parseSvg(parsed)